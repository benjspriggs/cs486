# CS486 Assignment 7
### Benjamin Spriggs

## Part 1 - Relational Algebra Equivalences

Each of the proposed equivalences below on relations `r(R)` and `s(S)` only holds under certain conditions. In the equivalences:

- `Z`, `R`, and `S` are **sets** of attributes
- `C` is a single attribute
- `COUNT(r)` returns the number of tuples in `r`
- `bow(x, y)` is a natural join

For each equivalence, give example schemas and instances for r
and s where the equivalence *does not hold*. (Assume that the expressions contain no syntax errors. For example, in ii, `Z` is assumed to be a subset of `R`.)

For each equivalence, give a side condition that guarantees the
equivalence will hold. The condition should be at the schema level (relation schemes,
keys, foreign keys) and not at the instance level (number of tuples, specific values).
The less restrictive the condition, the better.

### i

```
COUNT(bow(r, s)) = COUNT(r)
```

#### Response
```
r.schema = (_a_, b, c)
s.schema = (_c_, d, e)

r = [(1, 2, 3), (4, 5, 6)]
s = [(4, 5, 6), (5, 6, 7)]

COUNT(bow(r, s)) = 0
COUNT(r) = 2
```

If we add a condition on the common attributes in `r` that values must all appear in `s`, this condition will hold.


### ii

```
COUNT(bow(r, s)) = COUNT(r) * COUNT(s)
```

#### Response
```
r.schema = (a, b)
s.schema = (b, c)

r = [(1, 2), (2, 3)]
s = [(2, 4), (3, 6)]

COUNT(bow(r, s)) = 2
COUNT(r) * COUNT(s) = 2 * 2 = 4
```

If `r` and `s` have no common attributes between them, this condition will hold.

### iii

```
project_Z(bow(r, s)) = bow(project_Z(r), s)
```

#### Response
```
r.schema = (a, b)
s.schema = (b, c)

r = [(1, 2), (2, 3), (3, 4)]
s = [(2, 4), (3, 6)]

Z.schema = a

project_Z(bow(r, s)) = project_Z([(1, 2, 4), (2, 3, 6)])
	= [(1), (2)]

bow(project_Z(r), s) = bow([(1), (2), (3)], s)
	= cart([(1), (2), (3)], s)
```

If the projection `Z` includes rows that are in both `r` and `s`, the condition will hold.

### iv

```
project_Z(r - (select_{C=8}(r))) = project_Z(r) - project_Z(select_{C=8}(r))
```

#### Response

```
r.schema = (a, b)
r = [(1, 2), (1, 3), (2, 3), (2, 4)]

Z.schema = (a, b)
C = b == 3

select_Z(r - select_C(r)) = select_Z(r - [(1, 3), (2, 3)])
	= [(1, 2), (2, 4)]
project_Z(r) - project_Z(select_C(r)) =
	= r - [(1, 3), (2, 3)]
	= [(1, 2), (2, 4)]
```

This condition will always hold.


Because `C` is always a subset of `Z`, and `Z` is a projection of `r`,
this condition will always hold, since the projection will always take
elements in or out of `C` to their corresponding elements in `Z`.


## Part 2 - Statistics

### a
Determine the min and max salary values in the agent table, and the
number of rows in that table.

#### Response

```sql
SELECT min(salary), max(salary), COUNT(*) FROM agent;
```

| min   | max    | count |
|-------+--------+-------|
| 50008 | 366962 | 662   |


### b
Give an estimate for the number of rows in agent with `salary > 80000`,
assuming a uniform distribution of salaries between min and max salary. Explain how
you derived your estimate.

#### Response
```
prob(x) = 1/(b-a) = 1/(366962 - 50008)
366962 - 80000 = 286962
```

Since there are 286,962 values possible between our target and the max, 
we have approximately 90.5% of our values living in the range we're looking for,
which means we have 90.5% * 662 = 599.4 = 600 rows in that range.

### c
Find the 25th, 50th and 75th percentile values for salaries in the agent
table. (The 50th percentile value, for instance, is the smallest number `s`  such that 50% of the rows have salary value less than or equal to `s`.)

#### Response
```
.25 = x/(366962 - 50008) => x = 79171
.5  = x/(366962 - 50008) => x = 158342
.75 = x/(366962 - 50008) => x = 237513
```

Percentiles:

| 25th  | 50th   | 75th   |
|-------+--------+--------|
| 79171 | 158342 | 237513 |


### d
Give an estimate of the number of rows in agent with `salary > 80000`,
assuming in a uniform distribution in each quartile determined in c. Explain how you derived your estimate.

#### Response
```
prob(x_25)  = 1/4(79171 - 50008)
prob(x_50)  = 1/4(158342 - 79171)
prob(x_75)  = 1/4(237513 - 158342)
prob(x_100) = 1/4(366962 - 237513)
```

So we'll have all the values in the 75th and 100th percentiles, with some from the 50th.

```
prob_50  = (158342 - 80000)  * prob(x_50)
prob_75  = (237513 - 158342) * prob(x_75)
prob_100 = (366962 - 237513) * prob(x_100)

prob_50 + prob_75 + prob_100 = 0.68
```

So we could expect 68% percent of the rows would be in this range, `.68 * 662 = 450.16 = 451`.

### e
How many rows in agent actually have `salary > 80000`?

#### Response

```sql
SELECT COUNT(*) FROM agent WHERE salary > 80000;
```

197.

## Part 3 - Query Plans

For each SQL statement below, draw the query plan that Postgres chooses (which you can obtain with the EXPLAIN command).
For each plan, suggest a reason that the particular join algorithms were chosen.

#### Response

### a
```sql
SELECT A1.last, A2.last
FROM agent A1, agent A2, languagerel LR, Language L
WHERE A1.salary > A2.salary AND A2.agent_ID = LR.agent_ID
AND LR.lang_ID = L.lang_ID AND L.language = 'Korean';
```

#### Response
For the query plan and tree for this question, see the figure labeled `3a`.

![3a query plan](3a.png)


![3a tree](3a-tree.png)


##### Reasoning

Nested loops were probably chosen for the first join since the number of rows to consider in the first condition is essentially twice the size of agent (worst case).
The hash join was then chosen since we were checking equality, and needed to limit the number of rows again.

A bucket-based join was then chosen since we were checking equality for a string.
After that, we did everything in-memory since the number of rows to consider was small enough to be able to do something with them, and we could quickly verify other join conditions.

### b
```sql
SELECT A1.last, A2.last
FROM agent A1, agent A2, languagerel LR, Language L
WHERE A1.salary = A2.salary AND A2.agent_ID = LR.agent_ID
AND LR.lang_ID = L.lang_ID AND L.language = 'Korean';
```

#### Response
For the query plan and tree for this question, see the figure labeled `3b`.

![3b query plan](3b.png)

![3b tree](3b-tree.png)


##### Reasoning
We likely did a hash join first since we could throw those values (salary) quite quickly into buckets and proceed with what was left.

Hashes were used from then on since we were checking equality of values (most likely).

### c
```sql
SELECT A1.last, A2.last
FROM agent A1, agent A2, languagerel LR, Language L
WHERE A1.salary > A2.salary AND A2.agent_ID = LR.agent_ID
AND LR.lang_ID = L.lang_ID AND L.language <> 'Korean';
```

#### Response
For the query plan and tree for this question, see the figure labeled `3c`.

![3c query plan](3c.png)

![3c tree](3c-tree.png)


##### Reasoning

We started with a hash join since we optimized testing for equality before checking `salary` in `agent`.

Nested loops join was then used to determine relative values of `salary`.

Hash joins were used from then on since were are checking equality and non-equality of values, and that can be done quickly with buckets in-memory.


#### Rough Trees

![Rough diagram used for reference](trees.jpg)


## Part 4 - Recovery

Consider doing a 'batch commit' in a database system with write-ahead logging: the
recovery system waits until it has several transactions ready to commit, then writes all
their commit records to the disk at the same time (making sure to write out any log
records associated with those transactions first). Give one advantage of batch commit
and one disadvantage.

### Response

One advantage of batch commit with write-ahead logging is that queries tend to 'feel' faster and perform faster if you tune the actual committing of transactions of disk to happen when data isn't in use, or isn't being changed frequently. The strength of write-ahead is the ability to decide to wait in order to do a lot of I/O all at once, which can improve speed in terms of query responsiveness and data throughput.


One disadvantage of write-ahead logging is the event of crashing before the log is able to be committed to disk. Since the transaction batch lives in a cache or buffer somewhere, that data is more or less lost in the case of an electrical failure or power failure. Without writing the batch of commits to scratch or any other fallback, transactions would be left in an invalid state, and not up-to-date.

## Part 5 - Decomposition

Consider the following relational schema:
`violations(VID VT VD InD BID SNu SNa SCo Bro BrI Zip)`.
(The names are short for Violation ID, Violation Type, Violation
Description, Inspection Date, Building ID, Street Number, Street Name,
Street Code, Borough, Boro ID, Zip Code.)

Assume the following functional dependencies hold on `violations`:

- `VID => VT VD InD BID SNu SNa SCo Bro BrI Zip`
- `BID => SNu SNa SCo Bro BrI Zip`
- `Bro => BrI`
- `BrI => Bro`
- `VT => VD`
- `VD => VT`
- `SCo => SNa`
- `SNu Sco Zip => Bro`

### a
Show a decomposition of `violations` into BCNF. Show
each step in your decomposition and the keys for each relation scheme.
(You can have more than one key per scheme.)

#### Response
First, the attribute closure:
```
VID => VID VT VD InD BID SNu SNa SCo Bro BrI Zip
BID => BID SNu SNa SCo Bro BrI Zip
Bro => Bro BrI
BrI => BrI Bro
VT => VT VD
VD => VD VT
SCo => SCo SNa
SNu => SNu
Zip => Zip
```
`VID` is a candidate key for the relation.

Start:

1. Consider `violations`. `VID` is a candidate key, but there are transitive dependencies.
Break into relations based on `BID => SNu SNa SCo Bro BrI Zip`:
```
(VID VT VD InD BID)
(BID SNu SNa SCo Bro BrI Zip)
```

1. Consider `(VID VT VD InD BID)`. `VID` is a primary key, but there are transitive dependencies.
Break into relations based on `VT => VD`:
```
(_VID_ VT InD BID)*
(_VT_ VD)*
```

1. Consider `(BID SNu SNa SCo Bro BrI Zip)`. `BID` is a candidate key, but there are transitive dependencies.
Break into relations based on `SCo => SNa`:
```
(BID SNu SCo Bro BrI Zip)
(_SCo_ SNa)*
```

1. Consider `(BID SNu SCo Bro BrI Zip)`. `BID` is a candidate key, but there are transitive dependencies.
Break into relations based on `Bro => BrI`:
```
(BID SNu SCo Bro Zip)
(_Bro_ BrI)*
```

1. Consider `(BID SnU Sco Bro Zip)`. `BID` is a candidate key, but there are transitive dependencies.
Break into relations based on `SNu Sco Zip => Bro`:
```
(_BID_ SNu Sco Zip)*
(_SNu_ _Sco_ _Zip_ Bro)*
```

### b
Find a functional dependency in the original scheme that does
not correspond to a key in your normalized scheme.

#### Response
Consider the `VD => VT` relation. That functional dependency doesn't correspond to a key in the new, normalized schema, but still exists in the set of functional dependencies.
Same goes for `BrI => Bro`. That key relation got swapped from what I would have intuitively put as the ID.

### c
Is it possible that a legal (satisfies all keys) database instance `d`
on your normalized scheme violates the FD in b., in the sense that if you
join all the relations in `d` together, the result `r` violates the FD? Explain why
or why not.

#### Response
Because of how the `BID` and `SNu` relations are set up, we technically have two places where `Sco` and `Zip` are stored in the normalized relations. 

For instance, if we wanted to update the ZIP for a business, but not the street number or street code (for whatever reason) and we just issue an update to the `BID`-keyed table, we would join together a table with two conflicting ZIP entries.
The total instance would obey all the rules, but the result would violate the functional dependency with `Zip` in it, since there'd be two keys that lead to the same borough.

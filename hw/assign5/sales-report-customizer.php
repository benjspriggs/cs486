<?
require_once("common.php");

include_once('header.php');

$get = get(array(
	"price" => FILTER_SANITIZE_STRING,
	"order_count" => FILTER_SANITIZE_STRING,
	"total_sales" => FILTER_SANITIZE_STRING,
));

$price = empty($get) ? true : $get["price"];
$order_count = empty($get) ? true : $get["order_count"];
$total_sales = empty($get) ? true : $get["total_sales"];

?>

<h2>Orders Report Customizer</h2>

<form action="sales-report-customizer.php" method="get">
<div class="form-group">
<label for="price">Include price?</label>
<input <?= empty($price) ? '' : 'checked' ?> type="checkbox" id="price" name="price">
</div>
<div class="form-group">
<label for="order_count">Include order count?</label>
<input <?= empty($order_count) ? '' : 'checked' ?> type="checkbox" id="order_count" name="order_count">
</div>
<div class="form-group">
<label for="total_sales">Include total sales?</label>
<input <?= empty($total_sales) ? '' : 'checked' ?> type="checkbox" id="total_sales" name="total_sales">
</div>
<button class="btn btn-default" type="submit">Submit</button>
<button class="btn btn-default" type="reset">Reset</button>
</form>

<?

include_once("sales-report-fragment.php");

include_once('footer.html');

?>

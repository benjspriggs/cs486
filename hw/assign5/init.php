<?

require_once("common.php");

clean(function($conn) {
	foreach (array("sql/schema.sql", 
		"sql/resturaunt.foods.sql",
		"sql/resturaunt.customers.sql",
		"sql/resturaunt.beverages.sql",
		"sql/resturaunt.orders.sql") as $script) {
		$s = file_get_contents($script);
		_log("Running:");
		_log($s);

		pg_query($s) or die("Query failed for ${script}");
	}
});

?>

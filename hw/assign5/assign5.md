# CS486 - Assignment 5
### Benjamin Spriggs

## Notes

Because my assignment repo is public, I elected to *not* put my password/ username combo into version control.

Make a `creds.php` with:
```php
<?

define("USERNAME", "username");
define("PASSWORD", "hunter1");

?>
```

(I should have attached this in the response, but if it doesn't make it, this is assumed to be
there).

Data generated using <https://mockaroo.com>.

## Part 1
You are required to implement an example application using PHP for a
restaurant database similar to the one we had in HW4. For simplicity, we
will include only the following tables, and a customer order includes one
food item and one beverage.

```
(__Customer_ID__, Name)
(__Food_name__, price)
(__Beverage_name__, price)
(__Order_ID__, Customer_ID, Food_name, Beverage_name, tip)
```

Start by inserting some example customers, food items, and beverages using
the database GUI or the command line. (You do not need to implement a
page to insert data in these three tables).

Implement four web pages for the following features:
1. Insert order (10 points):
In this page, a restaurant employee can insert a new order for a
customer. The page should have two text boxes for customer ID and
order ID. The page also allows the end user to select a food item and
a beverage using a dropdown list. An example PHP code to construct
a dropdown list is posted in Piazza and can be accessed from the url:
<https://web.cecs.pdx.edu/~benotman/php486_3.php>
Another example about using HTML forms and PHP to insert data
was demonstrated during the lecture and posted in Piazza.
2. Item Sales Report (10 points):
Implement a second webpage to display a report for food item sales.
The report should look like the following table:

| Item Name    | Price | Number of orders | Total Sales |
|--------------+-------+------------------+-------------|
| French Fries | 1.5   | 10               | 15          |
| Cheese Buger | 3.5   | 14               | 49          |
| ...          |       |                  |             |

3. Customer Orders Report (15 points):
Implement a third webpage to display a report of all customers’
orders. List orders of each customer underneath the customer
information. The report should look like the following table:

| Customer ID                      | 1               | Customer Name | Sara Lindsay  |
|----------------------------------+-----------------+---------------+---------------|
| Food Item                        | Chicken Burrito | Beverage      | Coke 6oz      |
| Food Item                        | Cheese Burger   | Beverage      | Sprite 12oz   |
| Food Item                        | Tuna Salad      | Beverage      | Lemonade 8oz  |
| Food & beverage subtotal         |                 | 27            |               |
| Tip subtotal                     |                 | 6             |               |
| Total of food/ beverage and tips |                 | 33            |               |
|                                  |                 |               |               |
| Customer ID                      | 2               | Customer Name | Michael Lee   |
| Food Item                        | Cheese Burger   | Beverage      | Orange Juice  |
| Food Item                        | Fish n' Chips   | Beverage      | Diet Coke 8oz |
| Food & beverage subtotal         |                 | 17            |               |
| Tip subtotal                     |                 | 5             |               |
| Total of food/ beverage and tips |                 | 22            |               |
| ...                              |                 |               |               |

4. Item sales report customizer (10 points)
Implement a customization page for the item sales report in #2. The
page allows end users to select columns to include in the report. The
available column choices are price, number of orders, and total sales.
Food item name should always included. You can use an html form
with checkboxes to implement this page. This URL has an example:
<https://www.w3schools.com/tags/att_input_checked.asp>.

## Part 2

Question 2 (10 points): In the class examples, we have used
`pg_query_params` to avoid SQL injection attacks (instead of using
`pg_query`). Describe another way to prevent SQL injection attacks in PHP,
other programming languages, or database systems. Include an example
SQL injection attack, and explain how your method can prevent it . You
are free to consult any sources you want, but give the answer in your own
words, and cite any document or website you consult.

### Response

From the manual: <https://secure.php.net/manual/en/security.database.sql-injection.php>.

An example SQL injection, sourced from the manual:

```php
<?php

$offset = $argv[0]; // beware, no input validation!
$query  = "SELECT id, name FROM products ORDER BY name LIMIT 20 OFFSET $offset;";
$result = pg_query($conn, $query);

?>
```

Consider if the attacker provided a properly encoded SQL command in the URL?

Like:

```
http://mysite.com/search.php?offset='0;<...SQL...>'
```

This attacker would be able to grant themselves superuser access, or 
delete all data in the database: <https://xkcd.com/327/>.

There are a number of existing sources that go into detail on different SQL injection prevention strategies: <https://www.owasp.org/index.php/SQL_Injection_Prevention_Cheat_Sheet#Primary_Defenses>.

In short, you must:

* Assume anything coming from the user is malicious, sanitizing:
  1. Command inputs
  1. Query parameters
* Prepare statements using different language/ library schemes:
  1. `PreparedStatements` in Java
  1. `PreparedStatement` in C#
  1. Prepared statements in Golang <http://go-database-sql.org/prepared.html>

An example of a prepared statement in Java (<https://docs.oracle.com/javase/7/docs/api/java/sql/PreparedStatement.html>):
```java
Connection conn = DriverManager.getConnection("some url");
PreparedStatement pstmt = con.prepareStatement("UPDATE EMPLOYEES SET SALARY = ? WHERE ID = ?");
pstmt.setBigDecimal(1, 153833.00);
pstmt.setInt(2, 110592);
```

Preparing statements in this way avoids mistyped params, and will catch SQL strings embedded in provided parameters before they get executed against the server.

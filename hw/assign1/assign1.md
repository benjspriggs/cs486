# CS486 HW1

(Responses can be produced using the `make response` target and inspecting `output.dump`. The full text of each query is included before the response.)
  (PDF response can be produced using `make pdf`).

## Part 1
Give the English request that could have resulted in each of the  queries below (see Part Two for examples). (Don’t just paraphrase the SQL into words.) 
  Also include the *first five* rows of the result for each query (or fewer, the result is smaller), and the total number of rows returned.

### Questions

  1a.
  Select agents (with all available info in Agent) that live in Hong Kong, China.
  ```sql
  SELECT *
  FROM Agent
  WHERE city = 'Hong Kong'
  AND country = 'China';
  ```

  1b.
  Get all skills in the skill table.
  ```sql
  SELECT skill
  FROM skill;
  ```

  1c.
  Get all unique skills in the skill table.
  ```sql
  SELECT DISTINCT skill
  FROM skill;
  ```

  1d.
  Get the ID, city, contry of agents with salaries greater than 255,000, and that don't also live in France.
  ```sql
  SELECT agent_id, city, country
  FROM Agent
  WHERE salary > 255000
  AND country <> 'France';
  ```

  1e.
  Get the IDs and full name of all agents.
  ```sql
  SELECT agent_id, last, first, middle
  FROM Agent;
  ```

  2a.
  Get the titles and descriptions of all affiliations with an ID of 8.
  ```sql
  SELECT title, description
  FROM Affiliation
  WHERE aff_id = 8;
  ```

  2b.
  Get the titles and descriptions of all affiliations with an ID of 8.
  ```sql
  SELECT title, description
  FROM Affiliation
  WHERE Affiliation.aff_id = 8;
  ```

  2c.
  Get the titles and descriptions of all affiliations with an ID of 8.
  ```sql
  SELECT title, description
  FROM Affiliation A
  WHERE A.aff_id = 8;
  ```

  3a.
  Get all missions with security clearances where the classified mission was a success.
  ```sql
  SELECT *
  FROM Mission M, SecurityClearance S
  WHERE M.mission_status = 'success'
  AND S.sc_level = 'Classified';
  ```

  3b.
  Get all missions with security clearances where the classified mission was a success, and the access ID matches the security class ID.
  ```sql
  SELECT *
  FROM Mission M, SecurityClearance S
  WHERE M.mission_status = 'success'
  AND S.sc_level = 'Classified'
  AND M.access_id = S.sc_id;
  ```

  4a.
  Get the agent ID, first and last name, and city where that agent can speak Cherokee. Align agents and languages based on agent and language IDs.
  ```sql
  SELECT A.agent_id, A.first, A.last, A.city,
  A.country
  FROM Agent A, LanguageRel LR, Language L
  WHERE A.agent_id = LR.agent_id
  AND LR.lang_id = L.lang_id
  AND L.language = 'Cherokee';
  ```

  4b.
  Get agent first and last names, and city/ country pairs (distinct) where each pair of agent records:
  - They have the same city, country, salary
  - They have distinct clearance and agent IDs

  ```sql
  SELECT A1.first, A1.last, A2.city, A2.country
  FROM Agent A1, Agent A2
  WHERE A1.city = A2.city
  AND A1.country = A2.country AND A1.clearance_id <> A2.clearance_id
  AND A1.salary = A2.salary
  AND A1.agent_id <> A2.agent_id;
  ```

## Part 2
  Write a single  statement for each of the following queries. Show the first five rows
  of the result for each query (or fewer, if the result is smaller) and the number of rows
  returned. You should be able to write these  queries using only the features covered
  in the first lecture notes.

### Questions
  1. What are the affiliation ID and the description for ECHELON?
  ```sql
  SELECT aff_id, description FROM Affiliation WHERE title = 'ECHELON' LIMIT 5;
  ```

  1. What are the salaries of the Agents who have Unclassified or Classified clearance?
(Your query shouldn’t depend on what clearance IDs are used for these clearance levels,
 just the names of the levels.)
  ```sql
  SELECT a.salary, s.sc_level 
  FROM Agent as A, SecurityClearance as S 
  WHERE a.clearance_id = s.sc_id 
  AND s.sc_level = 'Unclassified' 
  OR s.sc_level = 'Classified'
  LIMIT 5;
  ```

  1. What are the ids and last names of all agents with at least two affiliations?
(You can do this without COUNT.)
  ```sql
  SELECT DISTINCT a.agent_id, a.first, a.last 
  FROM Agent as A, AffiliationRel as R, AffiliationRel as R1
  WHERE a.agent_id = r.agent_id
AND r.aff_id <> r1.aff_id
AND r.agent_id = r1.agent_id
  LIMIT 5;
  ```

  Comparison of count:
  ```sql
  SELECT a.agent_id, a.first, a.last 
  FROM Agent as A, AffiliationRel as R 
  WHERE a.agent_id = r.agent_id
GROUP BY a.agent_id
  HAVING COUNT(a.agent_id) > 2
  LIMIT 5;
  ```

  1. List the name and status of all missions that have at least one agent who has the LipReader skill. Don’t repeat missions in your result.
  ```sql
  SELECT DISTINCT m.name, m.mission_status
  FROM Mission as M, TeamRel as T, Agent as A, Skill as S, SkillRel as SR
  WHERE m.team_id = t.team_id
  AND t.agent_id = sr.agent_id
  AND sr.skill_id = s.skill_id
  AND s.skill = 'LipReader'
  LIMIT 5;
  ```

1. Which pairs of agents have the same first and last names? (List each pair only once.)
  ```sql
  SELECT DISTINCT a1.first as FirstAgent, a1.last as LastAgent, a2.first as First2Agent, a2.last as Last2Agent
  FROM Agent as A1, Agent as A2
  WHERE a1.last = a2.last AND a1.first <> a2.first
  LIMIT 5;
  ```

---
title: CS486 Notes
header-includes:
	<link rel="stylesheet" href="github-pandoc.css" >
---
| Topic                                   | Readings                                | Quiz | Homework |
|-----------------------------------------+-----------------------------------------+------+----------|
| 13.1-13.3, 14.1-14.3                    | Storage and Indexing, Join Algorithms   | 4    | 5        |
| 15.1 - 15.6, 16 intro, 16.2, 16.4, 16.5 | Query Optimization                      | 5    | 6        |
| 18.1-18.4; 19.2; 17.1-17.4              | Transactions, Concurrency Recovery      | 6    | (7)      |
| 3.1-3.4, 3.5.1; 10.1                    | Normalization, Security, Special Topics | 7    | 7        |

Compiled w/ help from Alexander Dmitriev.

# Links
- [Book](book.pdf)
- [Homeworks](hw)
- [Quizzes](quiz)
- [Lectures](lecture)
- [Tests](test)
- [Etc](etc)

# General Notes

Page offset is 39-1 for the 2nd edition (June 2008). Add 40 to get to the page you want.

15.2 has good notes on the different join algorithms.

## Disk properties

The question on the homework was on [homework 6](hw/hw06key.pdf).

### Readings
| Section | Page | Title                                      |
|---------+------+--------------------------------------------|
| 13.1    | 557  | The Memory Hierarchy                       |
| 13.1.1  | 557  | The Memory Hierarchy                       |
| 13.1.2  | 560  | Transfer of Data Between Levels            |
| 13.1.3  | 560  | Volatile and Nonvolatile Storage           |
| 13.1.4  | 560  | Virtual Memory                             |
| 13.2    | 562  | Disks                                      |
| 13.2.1  | 562  | Mechanics of Disks                         |
| 13.2.2  | 564  | The Disk Controller                        |
| 13.2.3  | 564  | Disk Acess Characteristics                 |
| 13.3    | 568  | Accelerating Access to Secondary Storage   |
| 13.3.1  | 568  | The I/O Model of Computation               |
| 13.3.2  | 569  | Organizing Data by Cylinders               |
| 13.3.3  | 570  | Using Multiple Disks                       |
| 13.3.4  | 571  | Mirroring Disks                            |
| 13.3.5  | 571  | Disk Scheduling and the Elevator Algorithm |
| 13.3.6  | 573  | Prefetching and Large-Scale Buffering      |


## Indexing

B+ trees are weird. Each row is an index where its children are
work like a normal binary tree, bottom row is all data, and all the
data is like a linear linked list.
Index nodes are bubbled based on the max number of nodes configured
for that specific tree.

![Clustered and unclustered B-Trees](btree.png)


### Clustered index example

| Index | Value |
|-------+-------|
| 1     | 1     |
| 2     | 2     |
| 3     | 3     |
| 4     | 4     |

### Unclustered index example

| Index | Value |
|-------+-------|
| 1     | 2     |
| 2     | 1     |
| 3     | 4     |
| 4     | 3     |

### Readings

| Section | Page | Title                                   |
|---------+------+-----------------------------------------|
| 14.1    | 620  | Index-Structure Basics                  |
| 14.1.1  | 621  | Sequential Files                        |
| 14.1.2  | 621  | Dense Indexes                           |
| 14.1.3  | 622  | Sparse Indexes                          |
| 14.1.4  | 623  | Multiple Levels of Index                |
| 14.1.5  | 624  | Secondary Indexes                       |
| 14.1.6  | 625  | Applications of Secondary Indexes       |
| 14.1.7  | 626  | Indirection in Secondary Indexes        |
| 14.1.8  | 628  | Document Retrieval and Inverted Indexes |
| 14.2    | 633  | B-Trees                                 |
| 14.2.1  | 634  | The Structure of B-trees                |
| 14.2.2  | 637  | Applications of B-trees                 |
| 14.2.3  | 639  | Lookup in B-trees                       |
| 14.2.4  | 639  | Range Queries                           |
| 14.2.5  | 640  | Insertion into B-trees                  |
| 14.2.6  | 642  | Delection from B-trees                  |
| 14.2.7  | 645  | Efficiency of B-trees                   |
| 14.3    | 648  | Hash Tables                             |
| 14.3.1  | 649  | Secondary-Storage Hash Tables           |
| 14.3.2  | 649  | Insertion into a Hash Table             |
| 14.3.3  | 650  | Hash-Table Deletion                     |
| 14.3.4  | 651  | Efficiency of Hash Table Indexes        |
| 14.3.5  | 652  | Extensible Hash Tables                  |
| 14.3.6  | 653  | Insertion into Extensible Hash Tables   |
| 14.3.7  | 655  | Linear Hash Tables                      |
| 14.3.8  | 657  | Insertion into Linear Hash Tables       |

## Operator implementation

Look at 5b in [the first exam](test/test1key.pdf) for an example implementation of semijoin.

The semijoin example is referenced in 2.4.8, or [page 43 (81) in the textbook](book.pdf#page=81).

## Relational equivalences

Look at [2.4](book.pdf#page=76) for a breakdown of relational inference rules.

Chapter 2 in general has more information on relations in general.

Look at [lecture 8](lecture/lecture08ink.pdf) for Mark-specific notes on relational equivalences.

Two specific examples [(slide 5-7, lecture 8)](lecture/lecture08ink.pdf#page=2):

1. Ability to break up multi-`and` select conditions into multiple, single `and` conditions.
1. "If you have a select operation following a join, and the
select condition applies ONLY to one of the tables then you can 'push' that select
operator before the join operator."

All equivalences are on slide 10, lecture 8 [(page 3 in the pdf)](lecture/lecture08ink.pdf#page=3).

### Equations

$$
\sigma_{c1 \land c2}(R) =  \sigma_{c1}(\sigma_{c2}(R))\\
\sigma_{c}(R\bowtie S) =  R \bowtie  \sigma_{c}(S)\\
R \bowtie S = S \bowtie R\\
R \bowtie (S\bowtie T) = (R \bowtie S)\bowtie T\\
\pi_{a}(R) =  \pi_a (\pi_{a1} (… \pi_{an}(R)))\\
\sigma_c (R \times S) = R \bowtie_{c} S\\
\pi_a (\sigma_c (R)) = \sigma_c (\pi_a (R)) - \text{if c ⊆ a}\\
\sigma_c (R \bowtie S) = R \bowtie (\sigma_c (S)) - \text{c uses attributes of S only}\\
(Q\bowtie S) ∪ (R\bowtie S) = (Q ∪ R)\bowtie S\\
$$

## Query plans and cost estimation

Look at [lecture 8](lecture/lecture08ink.pdf) for the full lecture on query plans and execution, along with cost estimation.

Block-oriented nested loops join is at [here in the book](book.pdf#page=766).
[Here](lecture/lecture07ink.pdf#page=14) in the lecture.

Page oriented nested loops join: [Here](lecture/lecture07ink.pdf#page=13) in the lecture.

### Equations

Cost of block-oriented nested loops join:
$$
= M + \frac{M}{B-2}*N
$$

If we're trying to consider the number of I/Os for a nested loops with this join ($M_{buffer}$ is the number of times we need to 'replenish' the buffer with fresh vals, T is the number of times we have to scan the inner table).
$$
= M_{buffer} + T_{scan} * N
$$

### Readings

| Section  | Page | Table                                                   |
|----------+------+---------------------------------------------------------|
| 15.1     | 703  | Introduction to Physical-Query-Plan Operators           |
| 15.1.1   | 703  | Scanning Tables                                         |
| 15.1.2   | 704  | Sorting While Scanning Tables                           |
| 15.1.3   | 704  | The Computation Model for Physical Operators            |
| 15.1.4   | 705  | Parameters for Measuring Costs                          |
| 15.1.5   | 706  | I/O Cost for Scan Operators                             |
| 15.1.6   | 707  | Iterators for Implementation of Physical Operators      |
| 15.2     | 709  | One-Pass Algorithms                                     |
| 15.2.1   | 711  | One-Pass Algorithms for Tuple-at-a-Time Operations      |
| 15.2.2   | 712  | One-Pass Algorithms for Unary, Full-Relation Operations |
| 15.2.3   | 715  | One-Pass Algorithms for Binary O peration               |
| 15.4     | 723  | Two-Pass Algorithms Based on Sorting                    |
| 15.4.1   | 723  | Two-Phase, Multiway Merge-Sort                          |
| 15.4.2   | 725  | Duplicate Elimination Using Sorting                     |
| 15.4.3   | 726  | Grouping and Aggregation Using Sorting                  |
| 15.4.4   | 726  | A Sort-Based Union Algorithm                            |
| 15.4.5   | 727  | A Sort-Based Intersection and Difference                |
| 15.4.6   | 728  | A Simple Sort-Based Join Algorithm                      |
| 15.4.7   | 729  | Analysis of Simple Sort-Join                            |
| 15.4.8   | 729  | A More Efficient Sort-Based Join                        |
| 15.4.9   | 730  | Summary of Sort-Based Algorithm                         |
| 15.5     | 732  | Two-Pass Algorithms Based on Hashing                    |
| 15.5.1   | 732  | Partitioning Relations by Hashing                       |
| 15.5.2   | 732  | A Hash-Based Algorithm for Duplicate Elimination        |
| 15.5.3   | 733  | Hash-Based Grouping and Aggregation                     |
| 15.5.4   | 734  | Hash-Based Union, Intersection, and Difference          |
| 15.5.5   | 734  | The Hash-Join Algorithm                                 |
| 15.5.6   | 735  | Saving Some Disk I/O                                    |
| 15.5.7   | 737  | Summary of Hash-Based Algorithm                         |
| 15.6     | 739  | Index-Based Algorithm                                   |
| 15.6.1   | 739  | Clustering and Nonclustering Indexes                    |
| 15.6.2   | 740  | Index-Based Selection                                   |
| 15.6.3   | 742  | Joining by Using an Index                               |
| 15.6.4   | 743  | Joins Using a Sorted Index                              |
| 16 intro | 759  |                                                         |
| 16.2     | 768  | Algebraic Laws for Improving Query Plans                |
| 16.2.1   | 768  | Commutative and Associative Laws                        |
| 16.2.2   | 770  | Laws Involving Selection                                |
| 16.2.3   | 772  | Pushing Selections                                      |
| 16.2.4   | 774  | Laws Involving Projection                               |
| 16.2.5   | 776  | Laws About Joins and Products                           |
| 16.2.6   | 777  | Laws Involving Duplicate Elimination                    |
| 16.2.7   | 777  | Laws Involving Grouping and Aggregation                 |
| 16.4     | 792  | Estimating the Cost of Operations                       |
| 16.4.1   | 793  | Estimating Sizes of Intermediate Relations              |
| 16.4.2   | 794  | Estimating the Size of a Projection                     |
| 16.4.3   | 794  | Estimating the Size of a Selection                      |
| 16.4.4   | 797  | Estimating the Size of a Join                           |
| 16.4.5   | 799  | Natural Joins With Multiple Join Attributes             |
| 16.4.6   | 800  | Joins of Many Relations                                 |
| 16.4.7   | 801  | Estimating Sizes for Other Operations                   |
| 16.5     | 803  | Introduction to Cost-Based Plan Selections              |
| 16.5.1   | 804  | Obtaining Estimates for Size Parameters                 |
| 16.5.2   | 807  | Computation of Statistics                               |
| 16.5.3   | 808  | Heuristics for Reducing the Cost of Logical Query Plans |
| 16.5.4   | 810  | Approaches to Enumerating Physical Plans                |

## Concurrency control

Schedules and concurrency control live in [lecture 9](lecture/lecture09ink.pdf).

Locks and similar ideas are **very** similar to OSes.

Serial execution is [here in lecture 9](lecture/lecture09ink.pdf#page=4).
Look at the order of commits, whichever goes into the release phase first is the first
transaction to actually commit, even if it doesn't start before the others.

### 2 Phase Locking Example
![2 Phase Locking](2pl.jpg)

### Readings

| Section | Page | Title                                      |
|---------+------+--------------------------------------------|
| 19.2    | 966  | Deadlocks                                  |
| 19.2.1  | 967  | Deadlock Detection by Timeout              |
| 19.2.2  | 967  | The Waits-for Graph                        |
| 19.2.3  | 970  | Deadlock Prevention by Ordering Elements   |
| 19.2.4  | 970  | Detecting Deadlocks by Timestamps          |
| 19.2.5  | 972  | Comparision of Deadlock-management methods |

## Recovery

Recovery and related info lives in [lecture 9](lecture/lecture09ink.pdf).

If we're trying to get the DB back into a known good state, we just have to either:

- Redo to the last committed transaction
- Undo to the last committed transaction (the one before the abort)

[Isolation levels.](lecture/lecture09ink.pdf#page=6)

### Readings
| Section | Page | Title                                     |
|---------+------+-------------------------------------------|
| 17.1    | 843  | Issues and Models for Resilient Operation |
| 17.1.1  | 844  | Failure Modes                             |
| 17.1.2  | 845  | More about Transactions                   |
| 17.1.3  | 846  | Correct Execution of Transactions         |
| 17.1.4  | 868  | The Primitive Operations of Transactions  |
| 17.2    | 851  | Undo Logging                              |
| 17.2.1  | 851  | Log Records                               |
| 17.2.2  | 853  | The Undo-Logging Rules                    |
| 17.2.3  | 855  | Recovery Using Undo Logging               |
| 17.2.4  | 857  | Checkpointing                             |
| 17.2.5  | 858  | Nonquiescent Checkpointing                |
| 17.3    | 863  | Redo Logging                              |
| 17.3.1  | 863  | The Redo-Logging Rule                     |
| 17.3.2  | 864  | Recovery With Redo Logging                |
| 17.3.3  | 866  | Checkpointing a Redo Log                  |
| 17.3.4  | 867  | Recovery With a Checkpointed Redo Log     |
| 17.4    | 869  | Undo/Redo Logging                         |
| 17.4.1  | 870  | The Undo/Redo Rules                       |
| 17.4.2  | 870  | Recovery with Undo/Redo Logging           |
| 17.4.3  | 872  | Checkpointing an Undo/Redo Log            |

## FDs, Decomposition, Normal Forms

All live in [lecture 10](lecture/lecture10.pdf).

BCNF decomposition in [lecture 10](lecture/lecture10.pdf#page=17). Make sure to start with calculating the attribute closure, or finding which attributes go where. If there's an attribute that maps to the entire relation, that's a possible key.

3NF implies BCNF, not the other way around. BCNF is lossy.

Covered in [homework 7](hw/hw07key.pdf#page=5).

[Superkey](book.pdf#page=109) = set of attributes with a key in it.

[Key](book.pdf#page=108) = Both of the following:

1. "Those attributes functionally determine all other attributes of the relation."
1. The key is minimal

MAKE SURE to not break apart keys when computing the closure. It doesn't work that way!
Need to keep keys together.

### Readings

| Section | Page | Title                                       |
|---------+------+---------------------------------------------|
| 3.1     | 67   | Functional Dependencies                     |
| 3.1.1   | 68   | Definition of Functional Dependencies       |
| 3.1.2   | 70   | Keys of Relations                           |
| 3.1.3   | 71   | Superkeys                                   |
| 3.2     | 72   | Rules About Functional Dependencies         |
| 3.2.1   | 72   | Reasoning About Functional Dependencies     |
| 3.2.2   | 73   | The Splitting/Combining Rules               |
| 3.2.3   | 74   | Trivial Functional Dependencies             |
| 3.2.4   | 75   | Computing the Closure of Attributes         |
| 3.2.5   | 77   | Why the Closure Algorithm Works             |
| 3.2.6   | 79   | The Transitive Rules                        |
| 3.2.7   | 80   | Closing Sets of Functional Dependencies     |
| 3.2.8   | 81   | Projecting Functional Dependenies           |
| 3.3     | 85   | Design of Relational Database Schemas       |
| 3.3.1   | 86   | Anomalies                                   |
| 3.3.2   | 86   | Decomposing Relations                       |
| 3.3.3   | 88   | Boyce-Codd Normal Form                      |
| 3.3.4   | 89   | Decomposition into BCNF                     |
| 3.4     | 93   | Decomposition: The Good, Bad, and Ugly      |
| 3.4.1   | 94   | Recovering Information from a Decomposition |
| 3.4.2   | 96   | The Chase Test for Lossless Joins           |
| 3.4.3   | 99   | Why the Chase Works                         |
| 3.4.4   | 100  | Dependency Preservation                     |


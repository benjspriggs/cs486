# CS486 HW6
### Benjamin Spriggs

## Part 1 - Disk Access

Consider a hard disk with the following characteristics:

| Average seek time | Average rotational delay | Page transfer time |
|-------------------+--------------------------+--------------------|
| 6ms               | 2ms                      | 0.03ms             |

Assume the disk has one platter with two surfaces.

### 1
Estimate the time to read a list of 100 pages from disk, where the pages are randomly distributed in tracks of a single cylinder.

#### Response
Pages are equivalent to blocks in the textbook, and disk I/O is covered in 13.2.3 (page 564) (for future reference @me).

We can estimate the time it takes, on average, for one block to be rotated to and read.

Since the blocks are randomly distributed, we can use the average rotational delay (since this is all on a single cylinder) and add the transfer time:

```
= 2 ms/page + 0.03 ms/page = 2.03 ms/page
```

So the total estimate is 100 times that, for the total number of pages:
```
= 100 pages * 2.03 ms/page = 203 ms
```

### 2
Estimate the time to read a list of 100 pages from disk, where the pages are randomly distributed across the disk.

#### Response
Let's estimate the time it takes for one page to be seek'd to, rotated to, and read.

Since the blocks are randomly distributed across the disk, we can use the average seek, delay and transfer times:
```
= 6 ms/page + 2 ms/page + 0.03 ms/page = 8.03 ms/page
```

So the total estimate, since we're seeking, rotating, and reading each block randomly (on average):
```
= 100 pages * 8.03 ms/page = 803 ms
```

### 3
Estimate the time to read a list of 100 pages from
disk, where the pages are arranged consecutively in tracks of a single
cylinder.

#### Response
We know that the blocks are arranged consecutively.
Assuming that the disk controller can read all of the pages at once, and we pay no overhead for reloading any caches, the first page will cost:
```
= (6 ms/page + 2 ms/page + 0.03 ms/page) * 1 page = 8.03 ms
```

And the remaining 99 pages will cost (since we're doing consecutive reads, the time between pages is negligible):
```
= 99 page * 0.03 ms/page = 2.97 ms
```

And the total time will cost:
```
= 8.03 ms + 2.97 ms = 11 ms
```


### 4
 Suppose we don’t care about the order in which the 100 pages are read. Suggest how to speed up the times for Questions 1 and 2 above.

#### Response

One way to speed up the times for both questions is to move to a solid-state drive or flash memory.

Another way would be to first arrange the list of pages in ascending memory order, so that the controller could serve up the pages as they appear on disk, and not as they're initially specified in the lsit.
Specifically for the second question, you might also arrange it so that pages appear in this new sorted list in ascending order and going from innermost cylinder to outermost cylinder, to minimize the time taken between seeks.

## Part 2 - Evaluating Queries with Indexes

Consider a database with two tables:
```
Pokemon(_charName_, attack, stamina, cType)
Captured(_charName_, _player_, difficulty)
```

Assume three unclustered indexes, where the leaf entries have the form
`[search-key value, RID]`:

1. `stamina` on `Pokemon
2. `cType` on `Pokemon`
3. `difficulty, charName` on `Captured

For Questions 5-12, say which queries can be evaluated with just data from
these indexes.
- If the query can, describe how.
- If the query can't, explain why.

### 5
```sql
SELECT charName, MIN(difficulty)
FROM Captured
GROUP BY charName;
```

#### Response
This query **can** be executed just using the index. We can:

1. Sort the index by difficulty, ascending
1. Eliminate duplicate rows (by `charName`)
1. Return the resulting set


### 6
```sql
SELECT AVERAGE(difficulty)
FROM Captured
GROUP BY player;
```

#### Response
This query **cannot** be executed just using the index.
This requires information with `player` to be indexed with `difficulty`. We don't have that index - so it can't be done.

### 7
```sql
SELECT MAX(difficulty)
FROM Captured;
```

#### Response
This query **can** be executed just using the index. We can:

1. Sort the index by difficulty, descending
1. Select the top row
1. Return the resulting set


### 8
```sql
SELECT cType, COUNT(charName)
FROM Pokemon
GROUP BY cType;
```

#### Response
This query **cannot** be executed just using the index.
This query requires that `charName` be indexed with `cType`. We're close to having that index, with the one on `Pokemon`, but we need to fetch each row in order to complete this query.

### 9
```sql
SELECT COUNT(*)
FROM Pokemon
WHERE stamina = 88 AND cType = 'Ice';
```

#### Response
This query **cannot** be executed just using the index.
This query requires that the `charName` be in both of the indexes on `Pokemon` in order for us to match up `stamina` and `cType`. Since we don't have that index, we would need to fetch `charName` for both qualifying indexes in order to complete the query.

### 10
```sql
SELECT cType, difficulty
FROM Pokemon, Captured
WHERE stamina = 77
 AND Pokemon.charName = Captured.charName;
```

#### Response
This query **can** be executed just using the index. We can (technically):

1. Select the rows from the `Captured` index with `stamina = 77`
2. Return the resulting set

Since the resulting set will do the natural join for us, we can safely ignore the `Pokemon` table entirely.

### 11
```sql
SELECT COUNT(DISTINCT stamina)
FROM Pokemon
WHERE cType = 'Ice';
```

#### Response
This query **can** be executed just using the index. We can:

1. Join the leaf entries for both indexes on `Pokemon` using `RID`
2. Select only the rows with `cType = 'Ice'`
3. Count the distinct `stamina` entries

### 12
```sql
SELECT charName, AVERAGE(difficulty)
FROM Captured
GROUP BY charName
HAVING COUNT(DISTINCT player) > 1;
```

#### Response
This query **cannot** be executed just using the index.
Because we don't have `player` information in any of our indexes, we can't reliably tell which rows meet the `HAVING` condition or not. We would need to fetch the rows that match the first few conditions in order to get that information.

## Part 3 - Operator Implementation
This question is about implementing outer join. Assume we have the tables
`Pokemon` and `Captured` from the previous part, and we want to compute
`Captured RIGHT OUTER JOIN Pokemon ON charName`.

### 13
Consider computing this join using a simple hash-join algorithm when `Captured` will fit in memory.
That is, it will be the inner input, which is inserted into a hash table.
Explain how to adapt the hash-join algorithm to produce the additional 
rows needed for the outer join.

#### Response
Since you'll be including rows in `Pokemon` that don't meet the equijoin condition, you could adapt the hash-join algorithm as following:

1. Put `Captured` in memory as a hash table (hashed on the join attribute, `charName`)
1. Parition an index on `Pokemon` so that each partition of rows can fit in memory
1. For each index in `Pokemon`:
	- If there's a matching index, add the merged rows to the result
	- If there's not a matching index, add just the current row in `Pokemon` to the result, filling out the rest of the columns that would be filled with the `Captured` row with `NULL`
1. Return the result set

### 14
Now suppose that `Pokemon` will fit in memory, but `Captured` won’t (so `Pokemon` will be the inner, hashed input).
Explain how to adapt the hash join to produce the additional rows.
Be sure to describe any additional information that the algorithm must maintain.

Note: Don’t confuse `INNER` and `OUTER` join options with the __inner__ and
__outer__ inputs to a join algorithm.

#### Response
Since you'll still be including rows in `Pokemon` that don't meet the equijoin condition, you can adapt the hash-join algorithm as following:

1. Put `Pokemon` in memory as a hash table (hashed on the join attribute, `charName`)
1. Generate an index on `Captured(charName)`
1. For each row in `Pokemon`:
	- Fetch the row in `Captured` with the same key
	- If there isn't a row, fill out the rest of the columns with `NULL` and add the row to the result
	- If there is a row, fill out the rest of the columns with the fetched row in `Captured` and add the row to the result
1. Return the result set

Better implementations of this algorithm might have batching of fetches such that each matching key isn't individually fetched, but fetched with rows that are adjacent to it.

<?
echo '<h2 id="orders-report">Orders Report</h2>';

echo '<table class="table">';
echo "<thead>";
echo "<tr>";

$headers = array(
	"price" => array(
		"display_as" => "Price",
		"select_with" => "price",
	),
	"order_count" => array(
		"display_as" => "Order Count",
		"select_with" => "count(order_id) as order_count",
	),
	"total_sales" => array(
		"display_as" => "Total Sales",
		"select_with" => "count(order_id) * price as total_sales",
	),
);

$get = get(array(
	"price" => FILTER_SANITIZE_STRING,
	"order_count" => FILTER_SANITIZE_STRING,
	"total_sales" => FILTER_SANITIZE_STRING,
));

$chosen = is_null($get) ? $headers : array_filter($headers, function($key) use($get) {
	_log($key);
	_log($get);
	return array_key_exists($key, $get) && isset($get[$key]);
}, ARRAY_FILTER_USE_KEY);

$displays = array_map(function($h){ 
	return $h["display_as"];
}, $chosen);
$selects = array_map(function($h){ 
	return $h["select_with"];
}, $chosen);

_log($displays);
_log($selects);
_log($chosen);

echo "<th>Food Name</th>";

foreach ($displays as $rh) {
	echo "<th>". $rh ."</th>";
}
echo "</tr>";
echo "</thead>";

rowIn('SELECT food_name ' . 
	(empty($selects) ? '' : ','. implode($selects, ',')) .
	' FROM resturaunt.foods
	NATURAL JOIN resturaunt.orders 
	GROUP BY food_name', function($r) {
		_log("new row...");
		_log(print_r($r, true));
		echo "<tr>". implode(array_map(function($val) use ($r) {
			echo "<td>". $val ."</td>" . PHP_EOL;
		}, $r)) . "</tr>". PHP_EOL;
	});

echo "</table>";
?>

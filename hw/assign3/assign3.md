# CS486 HW3
## Benjamin Spriggs

Code and responses can be found [here](https://gitlab.com/benjspriggs/cs486).

Responses can be generated using `make pdf`.

Limited output contained in the `Output` section,
full output contained in the `Explicit Output` section.

Look at the row count in the `Explicit Output` section for real counts, all of the output in `Output` will be limited to the first 5 rows.

In general:

| Operator        | Character             | Rel. Al. Meaning             |
|-----------------+-----------------------+------------------------------|
| `p_{...}(...)`  | Pi (no underscore)    | Project                      |
| `s_{...}(...)`  | Sigma (no underscore) | Select                       |
| `c(...)`        | Cross                 | Cross product                |
| `dp_{...}(...)` | Pi (underscore)       | Project, preserve duplicates |
| `b(...)`        | Bowtie                | Natural join                 |
| `&`             | Union                 | Union                        |

## Part 1

### 1
Write an equivalent SQL to the relational algebra expression:
```rel
p_{language}(s_{languagerel.lang_id = language.lang_id}
  (s_{languagerel.agent_id = agent.agent_id}
    (s_{skill.skill_id = skillrel.skill_id AND skill.skill = 'Computer Hacker'}
      (s_{skillrel.agent_id = agent.agent_id}
        (c(agent, skillrel, skill, languageel, language)))))
```

#### a
Using natural join:
```psql
-- 1a
SELECT DISTINCT language
FROM language
NATURAL JOIN languagerel
NATURAL JOIN agent
NATURAL JOIN skillrel
NATURAL JOIN skill
WHERE skill.skill = 'Computer Hacker';
```


#### b
Using inner join (or join):
```psql
-- 1b
SELECT DISTINCT language
FROM language
INNER JOIN languagerel USING(lang_id)
INNER JOIN agent USING(agent_id)
INNER JOIN skillrel USING(agent_id)
INNER JOIN skill USING(skill_id)
WHERE skill.skill = 'Computer Hacker';
```

#### c
Without a Join operator in the `FROM` clause:
```psql
--- 1c
SELECT DISTINCT l.language
FROM 
language as L,
languagerel as LR,
agent as A,
skillrel as SR,
skill as S
WHERE
L.lang_id = LR.lang_id AND
LR.agent_id = A.agent_id AND
SR.agent_id = A.agent_id AND
S.skill_id = SR.skill_id AND
S.skill = 'Computer Hacker';
```


### 2
Find the agent id and salary in Chinese Yuan for all agents whose
country is China. Name the result columns `China_ids` and `Yuan_pay`.
(Assume the salary column is in US dollars and one Yuan = 0.16 Dollar.)
```psql
SELECT agent_id as China_ids, salary / 0.16 AS Yuan_pay
FROM agent
WHERE country = 'China';
```

### 3
Find the high, low, and average salary for agents affiliated with CIA.
```psql
SELECT max(salary), min(salary), avg(salary)
FROM agent
NATURAL JOIN affiliationrel
NATURAL JOIN affiliation
WHERE title = 'CIA';
```

### 4
List agents with their teams, including agents with no teams. The result
should have last name, city, `agent_id`, and team name.
```psql
-- 4
SELECT agent_id, last, city, name
FROM agent
NATURAL JOIN teamrel
LEFT JOIN team ON team.team_id = teamrel.team_id;
```

### 5
List the team name for each team that has an agent who can speak
Japanese and an agent who has the Computer Hacker skill. Do this query
twice: Once using `INTERSECT` and once without using that operator.
```psql
-- 5a
(SELECT name
FROM team
NATURAL JOIN teamrel
NATURAL JOIN skillrel
NATURAL JOIN skill
WHERE skill = 'Computer Hacker'
)
INTERSECT
(SELECT name
FROM team
NATURAL JOIN teamrel
NATURAL JOIN languagerel
NATURAL JOIN language
WHERE language = 'Japanese'
);

-- 5b
SELECT DISTINCT name
FROM team
INNER JOIN
(SELECT team_id
FROM team
NATURAL JOIN teamrel
NATURAL JOIN skillrel
NATURAL JOIN skill
WHERE skill = 'Computer Hacker') as C USING(team_id)
NATURAL JOIN teamrel
NATURAL JOIN languagerel
NATURAL JOIN language
WHERE language = 'Japanese';
```

### 6
List agents who are in the White Crown or Black Country missions. Do
this query twice: Once using `UNION` and once without using that
operator.
```psql
-- 6a
(SELECT a.*
FROM agent A
NATURAL JOIN teamrel
NATURAL JOIN mission M
WHERE m.name = 'White Crown')
UNION
(SELECT a.*
FROM agent A
NATURAL JOIN teamrel
NATURAL JOIN mission M
WHERE m.name = 'Black Country');


-- 6b
SELECT A.*
FROM agent A
NATURAL JOIN teamrel
NATURAL JOIN mission M
WHERE m.name = 'White Crown'
OR m.name = 'Black Country';
```

## Part 2
The algebraic expressions below are for the database with schema
`pokemon(_Character_, CType, Stamina)` and `captured(_Location_, _Character_)`. 
For each proposed equivalence below, say whether it is true or false in general. 
Whenever your answer is "false", give an example instance of the database 
where the two expressions are not equal 
(and show the value of the expressions for your database
instance).

Use the following database instance:

*pokemon*

| Character  | CType  | Stamina |
|------------+--------+---------|
| Caterpie   | bug    | 90      |
| Metapod    | bug    | 100     |
| Charmander | fire   | 78      |
| Squirtle   | water  | 88      |
| Rattata    | normal | 60      |
| Raticate   | normal | 110     |

*Captured*

| Location | Character  |
|----------+------------|
| Bend     | Caterpie   |
| Bend     | Metapod    |
| Bend     | Rattata    |
| Bend     | Charmander |
| Canby    | Ekans      |
| Canby    | Caterpie   |

### 7
```rel
dp_{CType}(p_{CType, Stamina}(pokemon)) ?= p_{CType}(dp_{CType,Stamina}(pokemon))
```

This query is *true* in general.

### 8
```rel
dp_{CType,Location}(b(pokemon, captured)) ?= p_{CType, Location}(b(pokemon, captured))
```

This query is *false* in general.

Given a instance with duplicates in the type but not the name of the pokemon,
this query will return different results.

Say:

*pokemon*

| Character  | CType  | Stamina |
|------------+--------+---------|
| Caterpie   | bug    | 90      |
| Metapod    | bug    | 100     |

*Captured*

| Location | Character  |
|----------+------------|
| Bend     | Caterpie   |
| Bend     | Metapod    |

The first query would give you:


| CType | Location |
|-------+----------|
| bug   | Bend     |
| bug   | Bend     |

And the second would give:

| CType | Location |
|-------+----------|
| bug   | Ben      |

### 9
```rel
p_{character}(b(pokemon, captured)) ?= p_{Character}(pokemon) & p_{Character}(captured)
```

This query is *true* in general.

### 10
```rel
p_{Character}(s_{Location = 'Sandy'}(s_{Location = 'Canby'}(captured)))
?=
p_{Character}(s_{Location = 'Sandy'}(captured)) & p_{Character}(s_{Location = 'Canby'}(captured))
```

This query is *true* in general.

The first query would never yield a result, since the `Location` and `Character` form a primary key on `captured`.

Same for the second query.

## Part 3

The following questions use the Spy database. Write the SQL for the
following queries. Show the first five rows (or all the rows if fewer than
five) of the result for each query and the number of rows returned.

### 11
List the average salary for agents in each country.

```psql
SELECT country, AVG(salary) as Average_Salary
FROM agent
GROUP BY country;
```

### 12
List the number of agents and min and max salary
for agents in each country with 4 or fewer cities.

```psql
SELECT country, COUNT(agent_id), MIN(salary), MAX(salary)
FROM agent
GROUP BY country
HAVING COUNT(DISTINCT city) < 4;
```

### 13
List the number of agents who have each skill.
Include skill ID and the skill in the result.

```psql
SELECT skill_id, skill, COUNT(agent_id) AS Agent_Count
FROM skill
NATURAL JOIN skillrel
NATURAL JOIN agent
GROUP BY skill.skill_id;
```

### 14
List the agents (id, first, last) who have not been on
a ‘Top Secret’ mission that succeeded. Write this query two ways, once
using NOT EXISTS and once using NOT IN.

```psql
-- 14a
SELECT agent_id, first, last
FROM agent
WHERE NOT EXISTS 
(SELECT agent_id
FROM teamrel
NATURAL JOIN mission
NATURAL JOIN securityclearance
WHERE description = 'Top Secret'
AND mission_status = 'success');


-- 14b
SELECT agent_id, first, last
FROM agent
WHERE agent_id NOT IN
(SELECT agent_id
FROM teamrel
NATURAL JOIN mission
NATURAL JOIN securityclearance
WHERE description = 'Top Secret'
AND mission_status = 'success');
```

### 15
Find the skill and the number of agents for the
skills(s) with the most agents.

```psql
-- 15
SELECT skill, COUNT(agent_id) as Agent_Count
FROM skill
NATURAL JOIN skillrel
GROUP BY skill_id 
HAVING COUNT(agent_id) >= ALL (
SELECT COUNT(agent_id)
FROM skill
NATURAL JOIN skillrel
GROUP BY skill_id);
```

DROP SCHEMA if exists resturaunt CASCADE;

CREATE SCHEMA resturaunt;

CREATE TABLE resturaunt.customers (
	customer_id 	serial primary key,
	name		varchar(255)
);

CREATE TABLE resturaunt.foods (
	food_name	varchar(255) primary key,
	price		money
);

CREATE TABLE resturaunt.beverages (
	beverage_name	varchar(255) primary key,
	price		money
);

CREATE TABLE resturaunt.orders (
	order_id	serial primary key,
	tip		money,
	customer_id	integer references resturaunt.customers(customer_id),
	food_name	varchar(255) references resturaunt.foods(food_name),
	beverage_name	varchar(255) references resturaunt.beverages(beverage_name)
);

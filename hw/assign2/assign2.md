# CS486 HW2
## Benjamin Spriggs

### Responses
Responses can be generated with `make pdf`.
Comments can be used to locate matching queries.
Errors have been redirected to `errors.dump`, which is genrated by `make request`.

## Part 1

### 1
  Create the following table (Airlines) for five airline companies with the
AirLine column as the primary key. Insert the rows shown below. (3 points)

  | Airline           | Airline\_code | Headquarters        |
  |-------------------+---------------+---------------------|
  | Delta             | DL            | Atlanta, GA         |
  | Alaska            | AS            | Seattle, WA         |
  | American Airlines | AA            | Fort Worth, TX      |
  | Southwest         | SWA           | Dallas, TX          |
  | Aero Mexico        | AM            | Mexico City, Mexico |

  ```sql
  -- Create Airlines and insert data
  CREATE TABLE Airlines (
      Airline VARCHAR(255) PRIMARY KEY,
      AirlineCode VARCHAR(255) UNIQUE,
      Headquarters VARCHAR(255)
);
  INSERT INTO Airlines
(Airline, AirlineCode, Headquarters)
  VALUES
  ('Delta', 'DL',  'Atlanta, GA'),
  ('Alaska', 'AS',  'Seattle, WA'),
  ('American Airlines', 'AA',  'Fort Worth, TX'),
  ('Southwest', 'SWA',  'Dallas, TX'),
  ('Aero Mexico', 'AM',  'Mexico City, Mexico');
```


### 2
Create the following table (pdxFlights) (Schedule, Airline, FlightType,
    Arriving\_Departing, Status, FlightNo) with (FlightNo) as the primary
key. Also define the AirLine column as a foreign key referencing the
  Airlines table. Insert the following flight information for Portland
International Airport (PDX) on Jan 12. (3 points)

  | Schedule | Airline           | FlightType      | Arriving/Departing            | Status   | Flight |
  |----------+-------------------+-----------+-------------------------------+----------+--------|
  | 11:30 PM | Aero Mexico       | Departure | Mexico City                   | Departed | 497    |
  | 11:31 PM | Alaska            | Arrival   | San Francisco                 | Arrived  | 378    |
  | 11:33 PM | Alaska            | Arrival   | Seattle                       | Arrived  | 3402   |
  | 11:33 PM | Delta             | Arrived   | Salt Lake City                | Arrived  | 1674   |
  | 11:44 PM | American Airlines | Arrived   | Los Angeles                   | Arrived  | 6030   |
  | 11:45 PM | Southwest         | Arrival   | Lubbock, Las Vegas, San Jose  | Arrived  | 1375   |
  | 11:45 PM | Southwest         | Arrival   | Minneapolis/St. Paul, Phoenix | Arrived  | 1904   |
  | 11:46 PM | Delta             | Arrival   | Seattle                       | Arrived  | 4668   |
  | 11:55 PM | Southwest         | Arrival   | Denver                        | Arrived  | 445    |


  ```sql
  -- Create pdxFlights table and insert data
  CREATE TABLE pdxFlights (
      Schedule VARCHAR(255),
      Airline VARCHAR(255) REFERENCES Airlines(Airline),
      FlightType VARCHAR(255),
      Arriving_Departing VARCHAR(255),
      Status VARCHAR(255),
      FlightNo INT PRIMARY KEY
);
  INSERT INTO pdxFlights
(Schedule, Airline, FlightType, Arriving_Departing, Status, FlightNo)
  VALUES
  ('11:30 PM', 'Aero Mexico', 'Departure', 'Mexico City', 'Departed', 497),
  ('11:31 PM',  'Alaska', 'Arrival', 'San Francisco', 'Arrived', 378),
  ('11:33 PM',  'Alaska', 'Arrival', 'Seattle', 'Arrived', 3402),
  ('11:33 PM',  'Delta', 'Arrival',  'Salt Lake City', 'Arrived', 1674),
  ('11:44 PM',  'American Airlines', 'Arrival',  'Los Angeles', 'Arrived', 6030),
  ('11:45 PM',  'Southwest', 'Arrival',  'Lubbock, Las Vegas, San Jose', 'Arrived', 1375),
  ('11:45 PM',  'Southwest', 'Arrival',  'Minneapolis/St. Paul, Phoenix', 'Arrived', 1904),
  ('11:46 PM',  'Delta', 'Arrival',  'Seattle', 'Arrived', 4668),
  ('11:55 PM',  'Southwest', 'Arrival',  'Denver', 'Arrived', 445);
```

### 3
Modify your table to add two columns: Gate and Bag\_Carousel. Then
  update the existing rows in the table to add Gate and Bag\_Carousel
information using the following data. (3 points)

  | Schedule | Airline           | FlightType      | Arriving/Departing            | Status   | Flight | Gate | Bag Carousel |
  |----------+-------------------+-----------+-------------------------------+----------+--------+------+--------------|
  | 11:30 PM | Aero Mexico       | Departure | Mexico City                   | Departed | 497    | D12  | 4            |
  | 11:31 PM | Alaska            | Arrival   | San Francisco                 | Arrived  | 378    | C11  | 3            |
  | 11:33 PM | Alaska            | Arrival   | Seattle                       | Arrived  | 3402   | A12  | 2            |
  | 11:33 PM | Delta             | Arrived   | Salt Lake City                | Arrived  | 1674   | D7   | 8            |
  | 11:44 PM | American Airlines | Arrived   | Los Angeles                   | Arrived  | 6030   | C23  | 9            |
  | 11:45 PM | Southwest         | Arrival   | Lubbock, Las Vegas, San Jose  | Arrived  | 1375   | C13  | 4            |
  | 11:45 PM | Southwest         | Arrival   | Minneapolis/St. Paul, Phoenix | Arrived  | 1904   | C14  | 4            |
  | 11:46 PM | Delta             | Arrival   | Seattle                       | Arrived  | 4668   | D12  | 8            |
  | 11:55 PM | Southwest         | Arrival   | Denver                        | Arrived  | 445    | C18  | 4            |

  ```sql
  -- Update the table and insert
  ALTER TABLE pdxFlights
  ADD COLUMN Gate VARCHAR(255),
  ADD COLUMN BagCarousel VARCHAR(255);
-- There's a sanctioned way to do this, however
-- Postgres doesn't have a useful upsert, so
-- this table will be purged before we insert anything
DELETE FROM pdxFlights;
  INSERT INTO pdxFlights
(Schedule, Airline, FlightType, Arriving_Departing, Status, FlightNo, Gate, BagCarousel)
  VALUES
  ('11:30 PM', 'Aero Mexico', 'Departure', 'Mexico City', 'Departed', 497  ,  'D12', 4),
  ('11:31 PM',  'Alaska', 'Arrival', 'San Francisco', 'Arrived', 378   , 'C11', 3),
  ('11:33 PM',  'Alaska', 'Arrival', 'Seattle', 'Arrived', 3402  , 'A12', 2),
  ('11:33 PM',  'Delta', 'Arrival',  'Salt Lake City', 'Arrived', 1674  , 'D7', 8),
  ('11:44 PM',  'American Airlines', 'Arrival',  'Los Angeles', 'Arrived', 6030  , 'C23', 9),
  ('11:45 PM',  'Southwest', 'Arrival',  'Lubbock, Las Vegas, San Jose', 'Arrived', 1375  , 'C13', 4),
  ('11:45 PM',  'Southwest', 'Arrival',  'Minneapolis/St. Paul, Phoenix', 'Arrived', 1904  , 'C14', 4),
  ('11:46 PM',  'Delta', 'Arrival',  'Seattle', 'Arrived', 4668  , 'D12', 8),
  ('11:55 PM',  'Southwest', 'Arrival',  'Denver', 'Arrived', 445   , 'C18', 4);
```

### 4
  What happens if you insert the second row again? Explain why it
(does/does not) violate the constraints in the table definition. (3 points)

  | 11:31 PM | Alaska | Arrival | San Francisco | Arrived | 378 |

  Another insert of this row will create a row with a duplicated primary key (`FlightNo`). The query will fail with some kind of primary key error.
  ```sql
  -- Will fail the insert b/c of primary key constraint
  INSERT INTO pdxFlights
(Schedule, Airline, FlightType, Arriving_Departing, Status, FlightNo)
  VALUES
  ('11:31 PM',  'Alaska', 'Arrival', 'San Francisco', 'Arrived', 378);
  ```

### 5
  What happens if you insert the following flight information for JetBlue
  airlines? Explain why it (does/does not) violate the constraints in the
table definition. (3 points)

  | 11:57 PM | JetBlue | Arrival | Long beach | ARRIVED | 123 | D13 | 7 |

  Because there isn't a row with `'JetBlue'` in `Airlines`, this insert will fail the foreign key constraint on the table. `'JetBlue'` has to appear in the table for this insert to work.
  ```sql
  -- Will fail to insert b/c of foreign key constraint
  INSERT INTO pdxFlights
(Schedule, Airline, FlightType, Arriving_Departing, Status, FlightNo, Gate, BagCarousel)
  VALUES
  ('11:57 PM', 'JetBlue', 'Arrival', 'Long beach', 'ARRIVED', 123, 'D13', 7);
  ```

### 6
  What happens if you try to delete the following row from the Airlines
table? Explain what you see? (3 points)

  | AS | Alaska | Seattle, WA. |

  If you try to run this query, the row will attempt to delete, but because another table references that row for existing foreign keys, the DBMS will maintain referential integrity and not do it.
  A strategy for either updating rows that reference this foreign key needs to be provided.
  ```sql
  -- Perform DELETE that will fail the ref. trans. for the database
  DELETE FROM Airlines WHERE AirLine = 'Alaska';
  ```

### 7
  Write a query to retrieve flights departing to or arriving from Seattle. (3
      points)
  ```sql
  -- All flights in and out of Seattle
  SELECT * FROM pdxFlights
  WHERE LOWER(Arriving_Departing) LIKE LOWER('%Seattle%');
  ```

### 8
Write a query to retrieve the time of the earliest flight. (3 points)
  ```sql
  -- Time of earliest flight
  SELECT * FROM pdxFlights
  ORDER BY Schedule
  LIMIT 1;
  ```

### 9
Write a query to count the number of flights arriving to PDX. (3 points)
  ```sql
  -- Number of flights arriving to PDX
  SELECT COUNT(*) as "Number of flights arriving to PDX"
  FROM pdxFlights
  WHERE LOWER(FlightType) LIKE LOWER('%Arr%');
  ```

## Part 2
### 10
  Write the following queries in Relational Algebra, using ONLY the select,
  project and cross product operators:
- Find the city and country for all agents with first name Nick. (3 points)
  ```sql
  SELECT DISTINCT city, country
  FROM agent
  WHERE first = 'Nick';
  ```
  ```
  -- Project_{city, country}(Select_{first = 'Nick'}(agent))
  ```
- List agent id for of all agents who can speak German. (4 points)
  ```sql
  SELECT DISTINCT agent_id
  FROM agent
  NATURAL JOIN language
  NATURAL JOIN languagerel
  WHERE language = 'German';
  ```
  ```
  -- Project_{agent_id}(Select_{language = 'German'}(
  --  Select_{language.lang_id = languagerel.lang_id}(
  --  Select_{agent.agent_id = languagerel.agent_id}(
  -- Cross(agent, language, languagerel)))))
  ```
  - List the languages spoken by agents who have the Computer
Hacker skill. (4 points)
  ```sql
  SELECT DISTINCT language
  FROM agent
  NATURAL JOIN language
  NATURAL JOIN languagerel
  NATURAL JOIN skill
  NATURAL JOIN skillrel
  WHERE skill = 'Computer Hacker';
  ```
  ```
  -- Project_{language}(Select_{skill = 'Computer Hacker'}(
  --  Select_{agent.agent_id = languagerel.agent_id}(
  --    Select_{languagerel.lang_id = language.lang_id}(
  --      Select_{agent.agent_id = skillrel.agent_id}(
  --        Select_{skill_id = skill.skill_id}(
  --  Cross(agent, language, languagerel, skill, skillrel)))))))
  ```
### 11
  Write the following queries in Relation Algebra, using ONLY the select,
  project and join operators:
- Rewrite the query 10.c using join instead of cross product. (4 points)
  ```
  -- Project_{language}(Select_{skill = 'Computer Hacker'}(
  --  Join_{agent_id = languagerel.agent_id}(
  --    Join_{lang_id = language.lang_id}(
  --      Join_{agent_id = skillrel.agent_id}(
  --        Join_{skill_id = skill.skill_id}(
  --  Cross(agent, language, languagerel, skill, skillrel)))))))
  ```

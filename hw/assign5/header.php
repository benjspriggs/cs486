<!DOCTYPE html>
<html lang="en">
	<head>
		<title>CS486 - Benjamin Spriggs Assignment 5</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap-grid.min.css" integrity="sha256-ncgwwFXWgRIKh7g/Cqt9Pqph8dsoCIAk4GByMS/Y55I=" crossorigin="anonymous" />
		<link rel="stylesheet" href="https://cdn.rawgit.com/afeld/bootstrap-toc/v0.4.1/dist/bootstrap-toc.min.css">

		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>
		<script src="https://cdn.rawgit.com/afeld/bootstrap-toc/v0.4.1/dist/bootstrap-toc.min.js"></script>

		<style>
nav[data-toggle='toc'] {
	margin-top: 30px;
}

  /* small screens */
  @media (max-width: 768px) {
	  /* override the Affix plugin so that the navigation isn't sticky */
	  nav.affix[data-toggle='toc'] {
		  position: static;
	  }

	  /* don't expand nested items, which pushes down the rest of the page when navigating */
	  nav[data-toggle='toc'] .nav .active .nav {
		  display: none;
	  }
  }
		</style>

	</head>
	<body data-spy="scroll" data-target="#toc">
		<nav class="navbar navbar-default navbar-static-top">
			<div class="container">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbarSupportedContent" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>

				<a class="navbar-brand" href="#">CS486 - HW5</a>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav nav">
						<li class="nav-item">
							<a class="nav-link" href="index.html">Home</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="insert.php">Insert</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="sales-report.php">Sales Report</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="customer-report.php">Orders Report</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="sales-report-customizer.php">Sales Report Customizer</a>
						</li>
					</ul>

					<span class="navbar-text">Git rev: 
<a href="https://gitlab.com/benjspriggs/cs486/commit/<?= shell_exec("git rev-parse HEAD") ?>">Gitlab</a></span>
				</div>
			</div>
		</nav>

<main>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<nav id="toc" data-spy="affix" data-toggle="toc"></nav>
				</div>

				<div class="col-sm-9">


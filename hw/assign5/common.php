<?

require_once("creds.php");

define("URL", "dbclass.cs.pdx.edu");

function _log($msg) {
  echo '<div class="log" style="display: none;">'. 
    print_r($msg, true) .
    '</div>';
}

function connectionString() {
  return "host=". URL ." dbname=". USERNAME ." user=". USERNAME ." password=". PASSWORD;
}

// do things with an opened and closed connection
// return whatever $things returns
function clean($things) {
  $conn = pg_connect(connectionString()) or die("Database connection failed");

  $v = $things($conn);

  pg_close($conn);

  return $v;
}

// use a query and do some things on the results
function withQuery($q, $things) {
  return clean(function($conn) use ($q, $things) {
    _log($q);
    $result = pg_query($conn, $q);

    if (!$result) {
      _log("Error occured w/ the query '". $q ."'");
      _log(print_r(pg_last_error($conn)));
      return false;
    }
    return $things($result);
  });
}

// do something foreach row in a thing
function rowIn($q, $something) {
  return withQuery($q, function ($res) use ($something) {
    while ($row = pg_fetch_assoc($res)) {
      $something($row);
    }
  });
}

function option($value, $selected = false) {
  echo '<option value="'. $value .'"'. 
    ($selected == true ? 'selected' : '') . '>'. 
    $value .'</option>' . PHP_EOL;
}

function post($args) {
  return filter_input_array(INPUT_POST, $args);
}

function get($args) {
  return filter_input_array(INPUT_GET, $args);
}

function append(&$arr, $k, $val) {
  if (!array_key_exists($k, $arr)) {
    $arr[$k] = array();
  }

  array_push($arr[$k], $val);
}

?>

BEGIN {
	num = 1
}

/^```er$/ {
	flag=1
	next
}

/^```$/ && flag {
	flag=0
	++num
}

flag {
	print $0 > num ".er"
}



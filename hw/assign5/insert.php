<? 
require_once('common.php'); 

$post = post(array(
  "order_id" => FILTER_SANITIZE_NUMBER_INT,
  "customer_id" => FILTER_SANITIZE_NUMBER_INT,
  "food_name" => FILTER_SANITIZE_STRING,
  "beverage_name" => FILTER_SANITIZE_STRING,
));

function simpleDropdown($for, $in, $post, $selected = null) {
  echo '<select required name="' . $for . '">' . 
    '<option disabled selected value="">Select one...</option>';
  echo rowIn("SELECT $for FROM $in", function($r) use ($for, $post) {
    $val = $r[$for];
    return option($val, $val == $post[$for]);
  });
  echo PHP_EOL . '</select>'. PHP_EOL;
}

?>

<? include_once('header.php') ?>

<? 

if (!empty($post)) {
  $q = "INSERT INTO resturaunt.orders(order_id, customer_id, food_name, beverage_name)
    VALUES ($1, $2, $3, $4);";
clean(function($conn) use ($q, $post) {
  $row = array(
    $post["order_id"],    
    $post["customer_id"],    
    $post["food_name"],    
    $post["beverage_name"],    
  );
  _log($q);
  $result = pg_query_params($conn, $q, $row);

  if (!$result) {
    echo '<span>' .
      "Error occured w/ the query '". $q ."'" .
      print_r(pg_last_error($conn), true) .
      '</span>';
  } else {
    echo '<span>' .
      'Successfully inserted row ' .
      print_r($row, true) .
      '</span>';
  }

});
}
?>

<h2>Insert Order</h2>

<form action="insert.php" method="post">
<div class="form-group">
<label for="customer_id">Customer ID:</label>
<input class="form-control" required type="text" name="customer_id" value="<?= $post["customer_id"] ?>">
</div>
<div class="form-group">
<label for="order_id">Order ID:</label>
<input class="form-control" required type="text" name="order_id" value="<?= $post["order_id"] ?>">
</div>
<div class="form-group">
<label for="food_name">Food name:</label>

<? simpleDropdown("food_name", "resturaunt.foods", $post) ?>

</div>
<div class="form-group">
<label for="beverage_name">Beverage name:</label>

<? simpleDropdown("beverage_name", "resturaunt.beverages", $post) ?>

</div>
<button type="submit">Submit</button>
<button type="reset">Reset</button>
</form>
</div>
</main>

<? include_once('footer.html') ?>

# CS486 HW4
## Benjamin Spriggs

Response pdf can be produced with `make pdf`.
Requires `docker` to build the image that makes the magic ER diagrams to work.

I've left out most of the types since it would clutter my diagrams, but I'll include a table:

| Naming Convention         | Assumed Type                                                                                   |
|---------------------------+------------------------------------------------------------------------------------------------|
| `*id`                     | `SERIAL` in PostreSQL, or if it's referencing another table, the type of that table's id.      |
| `*_enum`, `*_type`        | `ENUM` with the provided values in the question.                                               |
| `name`                    | `VARCHAR(255)`.                                                                                |
| `feedback`, `description` | Whatever makes sense (`INT` if it's a star rating, or a `TEXT` for text feedback/ description) |

## Part 1

### 1
Consider the following scenario.
A restaurant wants to build a new system to support its walk-in customers.

- A customer is identified by a credit card number and card type (e.g.,
Visa or MasterCard). Every customer has a name.
- A food item (e.g., sandwich, soup, or fries) is identified by its name
and it has a price and number of calories. A food item can be a usual
side for another food item (e.g., fries is a usual side for a cheese
burger).
- A customer order can include one or more food items. The order is
identified by an id that is unique (no two customers can have a similar
order ids). The order must include the order date, the total amount of
the ordered food items, and the tip amount. A customer can have
multiple orders (when visiting the restaurant multiple times).
- A waiter is identified by social security number (SSN) and has a
name and age. The waiter main tasks are delivering an order to the
customer table and collecting the customer payment. A customer can
have two different waiters doing these two tasks for them.
- A chef is identified by SSN and has a name, age, and experience (# of
years). Only one chef cooks each food item. A chef can cook multiple food items.
- New chefs are usually supervised by an expert chef.

Draw an ER diagram that represents this scenario. You can use the
conventional notation (rectangles, diamonds, ovals) or the UML notation.
Be sure to mark the key attributes and include cardinality constraints on
relationships.

#### Response
```er
[customer]
	*ccnum {label:"PRIMARY KEY(ccnum, type)"}
	*type
	name
[food_item]
	*name
	price
	num_calories
[usual_side]
	*name
	side_name
[order]
	*id
	*cust_ccnum {label:"NOT NULL REFERENCES customer(ccnum)"}
	date {label:"NOT NULL"}
	total_amount {label:"NOT NULL"}
	tip_amount {label:"NOT NULL"}
[order_contents]
	*order_id
	*food_item {label:"NOT NULL REFERENCES food_item(name)"}
	*chef_ssn {label:"NOT NULL REFERENCES chef(ssn)"}
[waiter]
	*ssn
	name
	age
[reservation]
	*id
	*order_id {label:"NOT NULL REFERENCES order(id)"}
	*cust_ccnum {label:"NOT NULL REFERENCES customer(ccnum)"}
	*serving_waiter_ssn {label:"REFERENCES waiter(ssn)"}
	*collecting_waiter_ssn {label:"REFERENCES waiter(ssn)"}
[chef]
	*ssn
	*supervising_chef {label:"REFERENCES chef(ssn)"}
	name
	age
	num_years

customer 1--+ order
order 1--+ order_contents
food_item 1--? usual_side
waiter *--? reservation
food_item +--* order_contents
customer +--* reservation
reservation 1--1 order
chef 1--* order_contents
chef ?--* chef
```

![Response to #1](./1.png)

### 2
For Questions 2-5 you should modify your ER diagram from Question 1 to
handle the following changes in the scenario. Do each scenario as a separate
change from the original diagram. You only need to show the parts of the
diagram that change.

A customer can make a reservation which includes
the information: date, time, and number of expected visitors.

#### Response
You would just need to extend the `reservation` table, if we're not collecting everyone's credit card number:
```er
[reservation]
	*id
	*order_id {label:"NOT NULL REFERENCES order(id)"}
	*cust_ccnum {label:"NOT NULL REFERENCES customer(ccnum)"}
	*serving_waiter_ssn {label:"REFERENCES waiter(ssn)"}
	*collecting_waiter_ssn {label:"REFERENCES waiter(ssn)"}
	date
	time
	expected_visitors
```

![Response to #2](./2.png)

### 3
A food item can have different sizes and each size has
a different price and number of calories.

#### Response
You might keep a table of food item variants, and update any references to specific dishes to use the `variant_id`:
```er
[food_item]
	*name
[food_item_variant]
	*id
	*name {label:"NOT NULL REFERENCES food_item(name)"}
	price
	num_calories
[order_contents]
	*order_id
	*food_item {label:"NOT NULL REFERENCES food_item_variant(id)"}
	*chef_ssn {label:"NOT NULL REFERENCES chef(ssn)"}
```

![Response to #3](./3.png)

### 4
A customer can give feedback on his/her visit.

#### Response
Either as stars or as textual feedback, we can make a new table that refers to the `reservation` table:
```er
[rating]
	*id
	*reservation_id {label:"NOT NULL REFERENCES reservation(id)"}
	feedback
```

This table would have a many-to-one relationship with the `reservation` table.

![Response to #4](./4.png)

### 5
A customer can give feedback on specific food in
his/her order. The customer can also give feedback about the waiter who
brought the food and the waiter who collected the payment.

![Response to #5](./5.png)

#### Response
A similar extension to the previous question, this time referring to the `order_contents` table (with a many-to-one with the `order_contents` table):
```er
[food_rating]
	*id
	*order_content_id {label:"NOT NULL REFERENCES order_contents(id)"}
	feedback
[waiter_rating]
	*id
	*reservation_id {label:"NOT NULL REFERENCES reservation(id)"}
	feedback
	waiter_type {label:"WaitingRole NOT NULL"}
```

Since SQL doesn't allow for the kind of referencing we want to do with the `waiter_ssn` column, we have an enum to say which waiter did what:
```sql
CREATE TYPE WaitingRole AS ENUM ('serve', 'check');
```

![Response to #5](./5.png)

## Part 2
All of the next questions concern a TriMet driver entity (with driver license number,
name, and age). Each driver drives a specific vehicle type (e.g., bus,
streetcar, or max). The vehicle information includes type and maximum
speed. The driver can drive the vehicle for a specific line. The line has a
name, description, and length in miles. For example, the driver Jack Stewart
(license number: 12345 and age: 45) can only drive a bus (max speed: 50
mph) on only line 12 (description: Parkrose to Tigard, and length: 19 miles).

### 6
Give an ER diagram that represents a driver’s vehicle
and line information only using attributes (use only one entity).

#### Response
Clunky.
```er
[driver]
	*license_number
	name
	age
	vehicle_type
	max_vehicle_speed
	line_name
	line_description
	line_length
```

![Response to #6](./6.png)

### 7
Give an ER diagram that represents the information
using two entities, one relationship, and relationship attributes.

```er
[driver]
	*license_number
	name
	age

[vehicle]
	*driver_number {label:"NOT NULL REFERENCES driver(license_number)"}
	vehicle_type
	max_vehicle_speed
	line_name
	line_description
	line_length

driver 1--1 vehicle
```

![Response to #7](./7.png)

### 8
Give an ER diagram that represents the information
using a 3-way relationship.

#### Response
```er
[driver]
	*license_number
	name
	age

[vehicle]
	*id
	*driver_number {label:"NOT NULL REFERENCES driver(license_number)"}
	vehicle_type
	max_vehicle_speed

[line]
	*vehicle_id {label:"NOT NULL REFERENCES vehicle(id)"}
	line_name
	line_description
	line_length

driver 1--1 vehicle
vehicle 1--1 line
```

![Response to #8](./8.png)

### 9
Suppose a driver can work on multiple lines
(possibly with different starting dates). Which of your ER diagrams in
Questions 6-8 could represent this situation (possibly with a change of
cardinality constraints)?

#### Response

Definitely the last relationship. Maybe something like:

```er
[driver]
	*license_number
	name
	age

[vehicle]
	*id
	*driver_number {label:"NOT NULL REFERENCES driver(license_number)"}
	vehicle_type
	max_vehicle_speed

[line]
	*id
	*vehicle_id {label:"NOT NULL REFERENCES vehicle(id)"}
	name
	description
	length

driver 1--+ vehicle
vehicle 1--+ line
```

![Response to #9](./9.png)

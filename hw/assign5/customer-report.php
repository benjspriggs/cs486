<?
require_once("common.php");

include_once('header.php');

$mock = array(
  "order_id" => 20,
  "customer_id" => 1,
  "name" => "Bob",
  "tip" => 20.0,
  "subtotal" => 20.0,
  "total" => 40.0,
  "food_items" => array(
    "asdkfj",
    "asdkfja",
    "asdkja",
  ),
  "beverage_items" => array(
    "asdkfj",
    "asdkfja",
    "asdkja",
  ),
);

// $cust:
//   customer_id      => int
//   name             => string
//   tip              => float
//   subtotal         => float
//   total            => float
//   food_items       => [array(string)]
//   beverage_items   => [array(string)]
function customerRow($cust) {
  echo '<div id="'. $cust["customer_id"] . '" class="container">' .
      '<h3>Customer: '. $cust["name"] . '</h3>' .
      '<span>#'. $cust["customer_id"] . '</span>' .
      '<table class="table">' .
        '<tr>' .
          '<td>Food Items:<ul>' .
          implode(array_map(function ($item) {
            return '<li>' . $item .'</li>';
          }, $cust["food_items"])) .
          '</ul></td>' .
          '<td>Beverage Items:<ul>' .
          implode(array_map(function ($item) {
            return '<li>' . $item .'</li>';
          }, $cust["beverage_items"])) .
          '</ul></td>' .
      '</tr>' .
      '<tr><td>Subtotal:</td><td>' . $cust["subtotal"] . '</td></tr>' .
      '<tr><td>Tip: </td><td>' . $cust["tip"] . '</td></tr>' .
      '<tr><td>Total: </td><td>' . $cust["total"] . '</td></tr>' .
      '</table>' .
    '</div>';
}

?>

<h2 id="orders-report">Orders Report</h2>

<?
$rs = array();

rowIn("SELECT 
  customer_id, 
  name,
  sum(tip) as tip,
  sum(foods.price + beverages.price) as subtotal,
  sum(foods.price + beverages.price + tip) as total
  FROM resturaunt.orders 
  JOIN resturaunt.customers using(customer_id)
  JOIN resturaunt.foods using(food_name)
  JOIN resturaunt.beverages using(beverage_name)
  GROUP BY customer_id, name",
function($r) use(&$rs) {
  // _log(print_r($r, true));

  $rs[$r["customer_id"]] = $r;

  /*
  echo '<li><a href="#' . $r["customer_id"] . 
    '">' . $r["customer_id"] . 
    '</a></li>' . PHP_EOL;
   */
});

?>

<?

foreach($rs as $cid => $customer) {
  $customer["food_items"] = array();
  $customer["beverage_items"] = array();

  rowIn("SELECT
    food_name,
beverage_name
FROM resturaunt.orders
where customer_id = $cid",
function($r) use(&$customer) {
  append($customer, "food_items", $r["food_name"]);
  append($customer, "beverage_items", $r["beverage_name"]);
});

  customerRow($customer);

}

?>

<?

include_once('footer.html');

?>

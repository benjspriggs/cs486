#!/bin/bash

for file in {sql/resturaunt.beverages.sql,sql/resturaunt.customers.sql,sql/resturaunt.foods.sql}; do
  echo $file
  lines=`cat $file | grep -o "'.*'" | sort | uniq -d`
  for line in $lines; do
    sed -i "/$line/d" $file
  done
done
